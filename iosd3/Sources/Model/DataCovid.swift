//
//  DataCovid.swift
//  iosd3
//
//  Created by Student Supinfo on 20/06/2020.
//  Copyright © 2020 Student Supinfo. All rights reserved.
//

import Foundation

struct DataCovid {
    /*
    let country: String
    let slug: String
    let date: String*/
    
    
    let country: String

    let nCountryConfirmed: Int
    let tCountryConfirmed: Int
    let nCountryDeaths: Int
    let tCountryDeaths: Int
    let nCountryRecovered: Int
    let tCountryRecovered: Int
    
}

