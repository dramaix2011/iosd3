//
//  DataService.swift
//  iosd3
//
//  Created by Student Supinfo on 20/06/2020.
//  Copyright © 2020 Student Supinfo. All rights reserved.
//

import Foundation

class DataServices {
    
    func fetchData(completion: @escaping (_ dataCovid: [DataCovid] ) -> Void) {
        
        
        
        // - 1 get API endpoint (global countries and date data for covid-19)
        guard let url = URL(string:"https://api.covid19api.com/summary") else {return}
        
        // - 2 Header request no authentification needs
        var requestHeader = URLRequest(url: url)
        requestHeader.httpMethod = "GET"
        
        // - 3 Task creation
        let task = URLSession.shared.dataTask(with: requestHeader){ data, response, error in
            var datasCovid = [DataCovid]()
             
            // - 4 check != null
            if let data = data {
                
                do {
                    
                    // - 5 try to serialize to list of dictionaries
                    if let dictionary = try JSONSerialization.jsonObject(with: data) as? [String:Any] {
                        
                        // - 6 For each dictionnary we try to create a DataCovid
                       //if let Date = dictionary["Date"] as? String {
                            
                            if let countriesDictionary = dictionary["Countries"] as? [[String: Any]] {
                                
                                for dictionnary in countriesDictionary {
                                    if let Country = dictionnary["Country"] as? String, let newCountryConfirmed = dictionnary["NewConfirmed"] as? Int, let totalCountryConfirmed = dictionnary ["TotalConfirmed"] as? Int, let newCountryDeaths = dictionnary["NewDeaths"] as? Int, let totalCountryDeaths = dictionnary["TotalDeaths"] as? Int, let newCountryRecovered = dictionnary["NewRecovered"] as? Int, let totalCountryRecovered = dictionnary["TotalRecovered"] as? Int{
                                        let dataCovid = DataCovid( country: Country, nCountryConfirmed: newCountryConfirmed, tCountryConfirmed: totalCountryConfirmed, nCountryDeaths: newCountryDeaths, tCountryDeaths: totalCountryDeaths, nCountryRecovered: newCountryRecovered, tCountryRecovered: totalCountryRecovered)
                                        datasCovid.append(dataCovid)
                                  }
                                }
                            }
                            
                        
                        
                                
                                 /*
                             if let Country = countriesDictionary["Country"] as? String, let newCountryConfirmed = countriesDictionary["NewConfirmed"] as? Int, let totalCountryConfirmed = countriesDictionary ["TotalConfirmed"] as? Int, let newCountryDeaths = countriesDictionary["NewDeaths"] as? Int, let totalCountryDeaths = countriesDictionary["TotalDeaths"] as? Int, let newCountryRecovered = countriesDictionary["NewRecovered"] as? Int, let totalCountryRecovered = countriesDictionary["TotalRecovered"] as? Int{
                                let dataCovid = DataCovid( date: Date, country: Country, nCountryConfirmed: newCountryConfirmed, tCountryConfirmed: totalCountryConfirmed, nCountryDeaths: newCountryDeaths, tCountryDeaths: totalCountryDeaths, nCountryRecovered: newCountryRecovered, tCountryRecovered: totalCountryRecovered )
                                 datasCovid.append(dataCovid)

                             }*/
                            
                           

                           
                        
                        
                        

                    }
                    
                }catch let error {
                    print(error)
                }
                
            }
            
            // - 7 Call completion
            completion(datasCovid)
            
            
            /*guard let data = data else {
                
                print(String(describing: error))
                return
                
            }
            
            print(String(data: data, encoding: .utf8)!)
            */
            
        }
        task.resume()
        
        
    }
    
    
    
}
