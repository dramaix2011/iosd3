//
//  ListCountriesTableViewController.swift
//  iosd3
//
//  Created by Student Supinfo on 20/06/2020.
//  Copyright © 2020 Student Supinfo. All rights reserved.
//

import UIKit

class ListCountriesTableViewController: UITableViewController/*, DetailViewControllerDelegate */{

    // Properties
    var dataCovid: [DataCovid] = []
    
    var dataCovidSelected: DataCovid?
    let service = DataServices()
    
    // Lifecycle
    override func viewDidLoad() {
        super.viewDidLoad()
        
        service.fetchData { dataCovid in
            self.dataCovid = dataCovid
            DispatchQueue.main.async {
                self.tableView.reloadData()
            }
        }
        
    }

    // MARK: - Table view data source

    override func numberOfSections(in tableView: UITableView) -> Int {
        // #warning Incomplete implementation, return the number of sections
        return 1
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of rows
        return dataCovid.count
    }

    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath)
        
        if let cell = cell as? TableViewCell {
            
            cell.country.text = dataCovid[indexPath.row].country
            
            cell.tCountryConfirmed.text = String(dataCovid[indexPath.row].tCountryConfirmed)
            cell.tCountryDeaths.text = String(dataCovid[indexPath.row].tCountryDeaths)
            cell.tCountryRecovered.text = String(dataCovid[indexPath.row].tCountryRecovered)
            cell.nCountryConfirmed.text = String(dataCovid[indexPath.row].nCountryConfirmed)
            cell.nCountryDeaths.text = String(dataCovid[indexPath.row].nCountryDeaths)
            cell.nCountryRecovered.text = String(dataCovid[indexPath.row].nCountryRecovered)
            
        }

        // Configure the cell...

        return cell
    }
    

    /*
    // Override to support conditional editing of the table view.
    override func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        // Return false if you do not want the specified item to be editable.
        return true
    }
    */

    /*
    // Override to support editing the table view.
    override func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCell.EditingStyle, forRowAt indexPath: IndexPath) {
        if editingStyle == .delete {
            // Delete the row from the data source
            tableView.deleteRows(at: [indexPath], with: .fade)
        } else if editingStyle == .insert {
            // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
        }    
    }
    */

    /*
    // Override to support rearranging the table view.
    override func tableView(_ tableView: UITableView, moveRowAt fromIndexPath: IndexPath, to: IndexPath) {

    }
    */

    /*
    // Override to support conditional rearranging of the table view.
    override func tableView(_ tableView: UITableView, canMoveRowAt indexPath: IndexPath) -> Bool {
        // Return false if you do not want the item to be re-orderable.
        return true
    }
    */

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
