//
//  TableViewCell.swift
//  iosd3
//
//  Created by Student Supinfo on 20/06/2020.
//  Copyright © 2020 Student Supinfo. All rights reserved.
//

import UIKit

class TableViewCell: UITableViewCell {
    
    @IBOutlet weak var country: UILabel!
    
    
    @IBOutlet weak var tCountryConfirmed: UILabel!
    @IBOutlet weak var nCountryConfirmed: UILabel!
    @IBOutlet weak var tCountryDeaths: UILabel!
    @IBOutlet weak var nCountryDeaths: UILabel!
    @IBOutlet weak var tCountryRecovered: UILabel!
    @IBOutlet weak var nCountryRecovered: UILabel!
    @IBOutlet weak var pictureFavoris: UIImageView!
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        /*titleLabel.text = "Réunion"
        detailLabel.text = "10002"*/
        
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
